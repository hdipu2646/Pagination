using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Walid.System
{
    using UI;

    public class Pagination : MonoBehaviour
    {
        [Tooltip("Configuration Parameter")]
        [Header("Pagination Configuration")]
        public int currentPageNo = 1;
        [Range(5, 20)]
        public int maxPageShowing = 5;
        public int lastPageNo = 7;
        public UnityAction<int> LoadPage;
        public InputField inputPageNo;
        public List<PageButton> pageBtns;

        [Tooltip("Properties that hold and help to generate the Pagination")]
        [Header("Private Properties")]
        [SerializeField] private PageButton pageBtnPrefab;
        [SerializeField] private Transform pageBtnParent;

        #region Singleton
        private static Pagination instance;
        public static Pagination Singleton
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<Pagination>();
                    if (instance == null)
                    {
                        GameObject pagination = new GameObject();
                        pagination.name = typeof(Pagination).Name;
                        instance = pagination.AddComponent<Pagination>();
                    }
                }
                return instance;
            }
        }
        public static void SetNewSingleTon(Pagination pagination)
        {
            if (instance != null)
            {
                return;
            }
            instance = pagination;
        }
        #endregion Singleton

        #region Public Methods
        /// <summary>
        /// Paginate method is to generate a list of page numbers based on the given parameters. It handles different scenarios based on the relationship between the total number of pageBtns and the maximum number of page numbers to display.
        /// </summary>
        /// <param name="last">represents the total number of pageBtns.</param>
        /// <param name="maxListSize">represents the maximum number button with --- and page numbers to display at a time.</param>
        /// <param name="current">represents the current page number.</param>
        /// <returns></returns>
        public List<int> Paginate(int last, int maxListSize, int current)
        {
            List<int> pages = new List<int>(); // Create a new list to store the pageBtns
            int left, right; // Declare variables for the left and right page numbers
            if (last <= maxListSize) // If there are fewer pageBtns than the maximum number to display
                for (int i = 1; i <= last; i++)
                    pages.Add(i); // Add each page number to the list
            else if (current >= (last - (maxListSize / 2))) // If the current page is towards the end
            {
                pages.Add(1); // Always display the first page
                pages.Add(-1); // Use a special value (-1) to indicate a break in the page numbers
                left = last - (maxListSize - 3); // Calculate the starting page number to display on the left
                for (int i = left; i <= last; i++)
                    pages.Add(i);
            }
            else if (current <= (1 + ((maxListSize - 1) / 2))) // If the current page is towards the beginning
            {
                right = maxListSize - 2; // Calculate the ending page number to display on the right
                for (int i = 1; i <= right; i++)
                    pages.Add(i); // Add each page number to the list
                pages.Add(-1); // Use a special value (-1) to indicate a break in the page numbers
                pages.Add(last); // Always display the last page
            }
            else // If the current page is somewhere in the middle
            {
                left = current - ((maxListSize - 5) / 2); // Calculate the starting page number to display on the left
                right = current + ((maxListSize - 4) / 2); // Calculate the ending page number to display on the right
                pages.Add(1); // Always display the first page
                pages.Add(-1); // Use a special value (-1) to indicate a break in the page numbers
                for (int i = left; i <= right; i++)
                    pages.Add(i); // Add each page number to the list
                pages.Add(-1); // Use a special value (-1) to indicate a break in the page numbers
                pages.Add(last); // Always display the last page
            }
            return pages; // Return the list of page numbers
        }

        /// <summary>
        /// To Generate page buttons for pagination system, based on the current page considering with the last Page no and the maximum number of buttons to display
        /// </summary>
        /// <param name="current">Represents the current page number.</param>
        public void GeneratePageBtns(int current = 1)
        {
            Reset();
            currentPageNo = current;
            var pages = Paginate(last: lastPageNo, maxListSize: maxPageShowing + 2, current: currentPageNo);
            int length = pages.Count;
            for (int i = 0; i < length; i++)
            {
                PageButton pageBtn = Instantiate<PageButton>(original: pageBtnPrefab, parent: pageBtnParent);
                pageBtn.name = string.Format("Page {0}", pages[i] != -1 ? pages[i] : "---");
                pageBtn.PageNo = pages[i];
                if(LoadPage != null)
                    pageBtn.OnLoadPage.AddListener(LoadPage);
                if (current == pages[i])
                    pageBtn.Select();
                this.pageBtns.Add(pageBtn);
            }
        }

        /// <summary>
        /// Overloaded method of GeneratePageBtns takes a string parameter "currentStr" converts the string to an integer and then calls the integer parameter version
        /// </summary>
        /// <param name="currentStr">string parameter version converts the string to int page no</param>
        public void GeneratePageBtns (string currentStr = "1")
        {
            int current;
            int.TryParse(currentStr, out current);
            if(current > lastPageNo)
                current = lastPageNo;
            else if(current < 1)
                current = 1;
            inputPageNo.text = current.ToString();
            GeneratePageBtns(current: current);
            LoadPage?.Invoke(current);
        }

        public void Next()
        {
            GeneratePageBtns(current: currentPageNo + 1);
        }

        public void Previous()
        {
            GeneratePageBtns(current: currentPageNo - 1);
        }

        /// <summary>
        /// It generates a log message with the list of page numbers generated by the Paginate method.
        /// </summary>
        public void Debuging()
        {
            var pages = Paginate(last: lastPageNo, maxListSize: maxPageShowing, current: currentPageNo);
            int length = pages.Count;
            string log = string.Empty;
            for (int i = 0; i < length; i++)
                log = string.Format("{0},{1}", log, pages[i]);
            Debug.Log(log);
        }
        #endregion Public Methods

        #region Private Methods
        /// <summary>
        /// Reset the pagination component by removing all the page buttons that were generated in the previous pagination.
        /// </summary>
        private void Reset()
        {
            int length = pageBtns.Count;
            for (int i = 0; i < length; i++)
            {
                Destroy(pageBtns[i].gameObject);
            }
            pageBtns = new List<PageButton>();
        }
        #endregion Private Methods
    }
}