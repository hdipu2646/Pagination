﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


namespace Walid.System.UI
{
    [Serializable]
    public class PageLoadEvent: UnityEvent<int> { }

    public class PageButton : MonoBehaviour
    {
        public Button button;
        public Text btnText;
        public PageLoadEvent OnLoadPage;

        [SerializeField] private int pageNo;
        public int PageNo
        {
            get
            {
                return pageNo;
            }
            set
            {
                pageNo = value;
                button.interactable = pageNo != -1;
                btnText.text = pageNo != -1 ? string.Format("{0}", pageNo): "---";
                ColorBlock color = button.colors;
                color.disabledColor = pageNo != -1 ? button.colors.disabledColor : button.colors.normalColor;
                button.colors = color;
            }
        }

        public void Select()
        {
            button.interactable = false;
            ColorBlock color = button.colors;
            color.disabledColor = button.colors.selectedColor;
            button.colors = color;
        }

        public void LoadPage()
        {
            Pagination.Singleton.GeneratePageBtns(pageNo);
            OnLoadPage?.Invoke(pageNo);
        }
    }
}