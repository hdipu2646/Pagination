[v1.0.7](#v1.0.7)
### Added
- Sample File Upgraded
### Changes
- Page Loading from input Field is Fixed

[v1.0.6](#v1.0.6)
### Added
- Input Field Current Page Greatter than Last page validation added
### Changes
- Input Page No to Load page issue Fixed


[v1.0.5](#v1.0.5)
### Added

### Changes
- Change Page Loading Unity Action on GeneratePageBtns Methods to Global Action


[v1.0.4](#v1.0.4)
### Added
- Add Page Loading Unity Action on GeneratePageBtns Methods

### Changes


[v1.0.3](#v1.0.3)
### Added
- Successful Pagination version

### Changes
- Method Fixed
- Comment Added

[v1.0.2](#v1.0.2)
[v1.0.1](#v1.0.1)
[v1.0.0](#v1.0.0)